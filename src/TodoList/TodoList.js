import React, { Component } from "react";
import { connect } from "react-redux";
import { ThemeProvider } from "styled-components";
import { Button } from "./Components/Button";
import { Container } from "./Components/Container";
import { Dropdown } from "./Components/Dropdown";
import { Heading1, Heading2, Heading3, Heading4 } from "./Components/Heading";
import { Table, Th, Thead, Tr } from "./Components/Table";
import { TextField } from "./Components/TextField";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
} from "./Redux/Action/ToDoListActions";
import { add_task } from "./Redux/Types/ToDoListTypes";
import { arrTheme } from "./Theme/ThemeManager";

import { ToDoListDarkTheme } from "./Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "./Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./Theme/ToDoListPrimaryTheme";

class TodoList extends Component {
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task) => {
        return (
          <Tr key={task.id}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button className="ml-1">
                <i class="fa fa-edit    "></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i class="fa fa-check    "></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i class="fa fa-trash   "></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task) => {
        return (
          <Tr key={task.id}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i class="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  state = {
    taskName: "",
  };

  renderNewTask = () => {
    let { taskName } = this.state;
    let newTask = {
      id: Date.now(),
      taskName: taskName,
      done: false,
    };
    this.props.dispatch(addTaskAction(newTask));
  };

  renderTheme = () => {
    return arrTheme.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading1>To Do List</Heading1>
          <TextField
            onChange={(e) => {
              this.setState({
                taskName: e.target.value,
              });
            }}
            name="taskName"
            label="Task Name"
            className="w-50"
          />
          <Button onClick={this.renderNewTask} className="ml-2">
            <i className="fa fa-plus"></i>
            Add task
          </Button>
          <Button className="ml-2">
            <i class="fa fa-upload"></i>
            Update task
          </Button>
          <hr />

          <Heading2>Task to do</Heading2>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>

          <Heading2>Task completed</Heading2>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
  };
};

export default connect(mapStateToProps)(TodoList);
