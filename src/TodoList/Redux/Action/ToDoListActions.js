import {
  add_task,
  change_theme,
  delete_task,
  done_task,
} from "../Types/ToDoListTypes";

export const addTaskAction = (newTask) => ({
  type: add_task,
  newTask,
});
export const changeThemeAction = (themeId) => ({
  type: change_theme,
  themeId,
});

export const doneTaskAction = (taskID) => ({
  type: done_task,
  taskID,
});
export const deleteTaskAction = (taskID) => ({
  type: delete_task,
  taskID,
});
