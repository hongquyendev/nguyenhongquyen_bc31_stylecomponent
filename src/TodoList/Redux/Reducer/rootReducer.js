import { combineReducers } from "redux";
import { toDoListReducer } from "./MyToDoReducer";

export const rootReducer = combineReducers({
  toDoListReducer: toDoListReducer,
});
