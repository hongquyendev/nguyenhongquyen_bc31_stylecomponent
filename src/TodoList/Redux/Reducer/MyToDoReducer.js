import { arrTheme } from "../../Theme/ThemeManager";
import { ToDoListDarkTheme } from "../../Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Theme/ToDoListPrimaryTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
} from "../Types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task1", done: true },
    { id: 2, taskName: "task2", done: false },
    { id: 3, taskName: "task3", done: true },
    { id: 4, taskName: "task4", done: false },
  ],
};

export const toDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required!");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );

      if (index !== -1) {
        alert("Task name already exists!");
        return { ...state };
      }

      state.taskList = [...taskListUpdate, action.newTask];

      return { ...state };
    }
    case change_theme: {
      let theme = arrTheme.find((theme) => theme.id == action.themeId);

      if (theme) {
        state.themeToDoList = theme.theme;
      }
      return { ...state };
    }
    case done_task: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskID);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case delete_task: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id === action.taskID);
      if (index !== -1) {
        taskListUpdate.splice(index, 1);
        state.taskList = taskListUpdate;
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};
